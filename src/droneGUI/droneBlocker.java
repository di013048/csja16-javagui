package droneGUI;

/**
 * Class to create player entity.
 * @author Jamie
 * @version 1.5.2
 */

public class droneBlocker extends Drone {

	/**
	 * Constructor to set player entity position in xPlayerPos and yPlayerPos position with radiusPlayer radius
	 * Along with setting colour of entity
	 * @param xPlayerPos
	 * @param yPlayerPos
	 * @param radiusPlayer
	 */
	
    public droneBlocker(double xPlayerPos, double yPlayerPos, double radiusPlayer) {
        super(xPlayerPos, yPlayerPos, radiusPlayer);
        // Set colour variable to white
        colour = "WHITE";
    }
 
    /**
	 * Check drone method needed for abstract class, no functionality.
	 * @param adroneArenaObject
	 */

    @Override
    protected void droneAngleCheck(droneArena droneArenaObject) {}

    /**
	 * Adjust drone method needed for abstract class, no functionality.
	 */

    @Override
    protected void changeDronePos() {}
    
    /**
	 * Draw rectangle onto canvas with x, y position and rectange radius and colour
	 * @param droneCanvas
	 */
    
    public void drawDrone(droneCanvas droneCanvas) {
        droneCanvas.drawRectangle(x, y, radian, colour);
    }
    
    /** 
     * Returns Player string to display on object
     * @return uid & Player
     */
    protected String getEntityString() {
        return droneUID + " Player";
    }
}

package droneGUI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Class interface to display arena, arena information, tool bar and file options
 * @aurhor Jamie
 * @version 1.5.2
 */

public class droneInterface extends Application {
	// Initialise drone canvas object
    private droneCanvas droneCanvasObject;
    // Initialise animation timer 
    private AnimationTimer timer;
    // Initialise information panel for drone list information
    private VBox informationPanel;
    // Initialise arena object from droneArena lcass
    private droneArena arena;
    
    /**
     * 
     * about pop up box that displays short messsage
     */
    private void aboutDialogBox() {
    	// Declare dialog as new alert box
        Alert aboutUsDialog = new Alert(AlertType.INFORMATION);
        // Set title of dialog box
        aboutUsDialog.setTitle("ABOUT");
        // Set header title of dialog box
        aboutUsDialog.setHeaderText("Developed by student 28013048");
        // Set text as content
        aboutUsDialog.setContentText("Drone simulation game");
        // Display about box
        aboutUsDialog.showAndWait();
    }
    
    /**
     * Method for mouse event to move
     * Player to click position
     * @param canvas
     */
    public void MouseClick (Canvas droneCanvasObject) {
    	// When mouse is clicked
    	droneCanvasObject.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
    		public void handle(MouseEvent e) {
    			// Set e object to mouse click position
    			arena.playerInstance(e.getX(), e.getY());
    			// Draw drone information panel and draw a new world
                drawInformationPanel(); drawWorld();
            }
    	});
    }
    
    /**
     * Create menu including the options and functionality
     * @return menuBar
     */
    MenuBar setMenu() {
    	// Initialise new menu object
        MenuBar mainMenu = new MenuBar();
        // Initialise menu called File
        Menu fileMenu = new Menu("FILE");
        // Create save sub menu
        MenuItem saveMenu = new MenuItem("SAVE");
        saveMenu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
            	// Call to save arena
            	saveArena();
            }
        });

        // Create load sub menu
        MenuItem loadMenu = new MenuItem("LOAD");
        loadMenu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
            	// Call to load arena
                loadArena();
            }
        });
        
        // Initialise sub menu called exit
        MenuItem fileExit = new MenuItem("EXIT");
        // On click
        fileExit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
            	// Stop animation timer
                timer.stop();
                // Exit application
                System.exit(0);
            }
        });
        
        fileMenu.getItems().addAll(saveMenu, loadMenu, fileExit);
        
        // Initialise menu called help
        Menu helpMenu = new Menu("HELP");
        // Initialise sub menu about
        MenuItem aboutMenu = new MenuItem("ABOUT");
        // On button click
        aboutMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	// Display about dialog box
            	aboutDialogBox();
            }
        });
        // Append about us sub menu to help menu
        helpMenu.getItems().addAll(aboutMenu);
        // Set file menu and help menu to main file bar
        mainMenu.getMenus().addAll(fileMenu, helpMenu);
        // Return main menu
        return mainMenu;
    }

    /**
     * Create horizontal tool bar
     * set up the horizontal box for the bottom with relevant buttons
     * @return
     */
    private HBox setButtons() {
    	// Initialise play button
        Button playButton = new Button("Start");
        // On button click start timer
        playButton.setOnAction(click -> timer.start());
        // Initialise pause button
        Button pauseButton = new Button("Pause");
        // On button click stop timer
        pauseButton.setOnAction(click -> timer.stop());
        // Initialise add drone button
        Button addDroneButton = new Button("Add drone to arena");
        // On button click add drone to arena and redraw arena
        addDroneButton.setOnAction(click -> {
        	arena.droneAdder();
        	drawWorld();
        });

        // Return horizontal tool bar with play, pause and add buttons
        return new HBox(playButton, pauseButton, addDroneButton);
    }

    /**
     * Draw the heatlh status onto entity
     * @param entityXPos
     * @param entityYPos
     * @param health
     */
    public void showScore(double entityXPos, double entityYPos, int health) {
    	droneCanvasObject.drawText(entityXPos, entityYPos, Integer.toString(health));
    }

    /**
     * Display information panel describing entities and their positions
     */
    public void drawInformationPanel() {
    	// Clear information panel
        informationPanel.getChildren().clear();
        // Initialise drone array list
        ArrayList<String> droneArrayList = arena.droneCollation();
        // Iterate over drone list
        for (String element : droneArrayList) {
        	// Initialise label from drone string
            Label l = new Label(element);
            // Add label to information panel
            informationPanel.getChildren().add(l);
        }
    }

    /**
     * Method to start application and set to set up toolbars and canvas
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
    	// Set scene title
        primaryStage.setTitle("28013048");
        // Apply border pane and add padding
        BorderPane bp = new BorderPane();
        bp.setPadding(new Insets(10, 20, 10, 20));
        // Set top menu
        bp.setTop(setMenu());
        // Initialise root scene object
        Group root = new Group();
        // Initialise new canvas with width and height
        Canvas canvas = new Canvas(500, 500);
        // Add canvas to root group
        root.getChildren().add(canvas);
        // Load canvas onto right side of screen
        bp.setRight(root);
        // Initialise drone canvas object as new drone canvas with width and height
        droneCanvasObject = new droneCanvas(canvas.getGraphicsContext2D(), 500, 500);
        // Call to mouse events
        MouseClick(canvas);        
        // Initialise drone arena using width and height size
        arena = new droneArena(500, 500);
        drawWorld();
        
        // Declare timer using animation timer
        timer = new AnimationTimer() {
            public void handle(long currentNanoTime) {
            	// Change drone position in arena
                arena.changeDronePosition();
            	// Change drone angles in arena
                arena.droneAnglesCheck();
                // Draw status menu
                drawInformationPanel();
                // Draw world
                drawWorld();
            }
        };

        // Set information panel to vertical box
        informationPanel = new VBox();
        // Set top left alignment of information panel
        informationPanel.setAlignment(Pos.TOP_LEFT);
        // Add padding to information panel
        informationPanel.setPadding(new Insets(5, 75, 75, 5));
        // Set left alignment for information panel
        bp.setLeft(informationPanel);
        // Add bottom tool bar buttons to bp
        bp.setBottom(setButtons());
        // Declare scene variable using bp
        Scene scene = new Scene(bp, 800, 600);
        // Declare scene height and width
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());
        // Set primary stage scene using object
        primaryStage.setScene(scene);
        // Show primary scene
        primaryStage.show();
    }
    
    /**
     * Draw a new arena
     */
    public void drawWorld() {
    	// Clear canvas
        droneCanvasObject.clearCanvas();	
        // Draw arena to arena from canvas
        arena.drawArenaToCanvas(droneCanvasObject);
    }
    
    /**
     * Method to save the simulation to the file
     */
    public void saveArena() {
    	// Initialise random object
    	Random guid = new Random();
    	// Declare unique file location
    	String file = "SimCache/simSave-"+guid.nextInt()+".txt";
    	try {
    		// Declare output stream to file location
    		ObjectOutputStream fileChoice = new ObjectOutputStream(new FileOutputStream(file));
    		// Write arena object to file
    		fileChoice.writeObject(arena);
    		// Close file output stream
    		fileChoice.close();
    	} catch(Exception e) { // Catch error
    		System.out.println(e); // Output error
    	}
    }
    
    /**
     * Method to load the simulation from a saved text file
     */
    public void loadArena() {
    	JFileChooser fileDialog = new JFileChooser(FileSystemView.getFileSystemView());
		// Initialise File object with current path + ArenaStorage
		File arenaDirectory = new File(System.getProperty("user.dir"),  "simCache");
		// Set title to choose a building to load
		fileDialog.setDialogTitle("Choose an arena to load");
		// Set file selection to files only
		fileDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		// set file explorer directory
		fileDialog.setCurrentDirectory(arenaDirectory);
		// Open file dialog
        int fileChoice = fileDialog.showOpenDialog(null);
        // If approve button is selected
        if (fileChoice == JFileChooser.APPROVE_OPTION) {
        	// Initialise file object with selected file
            File file = fileDialog.getSelectedFile();
            // Create scanner object
            try {
            	// Get file location
        		ObjectInputStream loader = new ObjectInputStream(new FileInputStream(file));
        		// Read file contents into arena variable
        		arena = (droneArena) loader.readObject();
        		// Close file reader
        		loader.close();
        		// Load world
        		drawWorld();
        	} catch(Exception e) { // Catch error
        		System.out.println(e); // Output error
        	}
        }
    } 

    /**
     * Main method to launch user interface
     * @param args
     */
    public static void main(String[] args) {
    	// Launch user interface
        Application.launch(args);
    }
}

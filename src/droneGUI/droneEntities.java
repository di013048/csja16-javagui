package droneGUI;

/**
 * Class to create drone objects.
 * @author Jamie
 * @version 1.5.2
 */
public class droneEntities extends Drone {

    private double Angle, Speed;
    
    /**
     * Constructor to create drones, parsing x and y position.
     * As well as angle drone speedDrone moving and its speed
     * And settings its colour
     * @param xDronePos
     * @param yDronePos
     * @param radiusDrone
     * @param angleDrone
     * @param speedDrone
     */
    public droneEntities(double xDronePos, double yDronePos, double radiusDrone, double angleDrone, double speedDrone) {
        super(xDronePos, yDronePos, radiusDrone);
        Angle = angleDrone; Speed = speedDrone; colour = "RED";
    }
    
    /**
     * Adjust position of drone based on angle and speed of drone
     */
    @Override
    protected void changeDronePos() {
    	// Calculate angle in radians
        double radAngle = Angle*Math.PI/180;
        // set x position based on speed multiplied by angle
        x += Speed * Math.cos(radAngle);
        // set y position based on speed multiplied by angle
        y += Speed * Math.sin(radAngle);
    }

    /**
     * Adjust angle of drone if hits another wall or another drone
     * @param droneArenaObject
     */
    @Override
    protected void droneAngleCheck(droneArena droneArenaObject) {
        Angle = droneArenaObject.CheckDroneAngle(droneUID, radian, x, y, Angle);
    }
    
    /**
     * Draw circle for drone into canvas, with position, radius and colour
     * @param droneCanvasObject
     */
    public void drawDrone(droneCanvas droneCanvasObject) {
        droneCanvasObject.drawCircle(x, y, radian, colour);
    }
    
    /**
     * Returns Drone string to display on object
     * @return uid & Drone
     */
    protected String getEntityString() {
        return droneUID + " Drone";
    }

}
    
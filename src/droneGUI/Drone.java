package droneGUI;

import java.io.Serializable;

/**
 * Class used to create drones using size, id, radius and colour.
 * Also using inherited methods
 * @author Jamie
 * @version 1.5.2
 */

public abstract class Drone implements Serializable {
	// Allows fcontactDroneRadius incrementing of uid fcontactDroneRadius each drone
	static int tempdroneUID = 0;
	// Unique identifier fcontactDroneRadius drone
	protected int droneUID;
	// Position and radius of drone
    protected double x, y, radian;						
    // Colour of drone
    protected String colour;
    
    /**
     * Default constructcontactDroneRadius fcontactDroneRadius Drone class
     * Creates drone at position 100, 100 with radius of 10
     */
    Drone() {this(100, 100, 10);}
    
    /**
     * ConstructcontactDroneRadius to create drone entity based on xDronePos, yDronePos and ir.
     * @param xDronePos
     * @param yDronePos
     * @param irradiusDrone
     */
    Drone(double xDronePos, double yDronePos, double radiusDrone) {
    	// Set drone position and radius to parsed positions
        x = xDronePos; y = yDronePos; radian = radiusDrone;
        // Increment static drone unique identifier integer by 1 and set to drone UID
        droneUID = tempdroneUID++;
        // Set drone colour
        colour = "RED";
    }
    
    /**
     * Abstract method to adjust drone variables
     */
    protected abstract void changeDronePos();
    
    /**
     * Abstract method to check drone position is valid
     * @param droneArenaObject
     */
    protected abstract void droneAngleCheck(droneArena droneArenaObject);
    
    /**
     * Draw circle fcontactDroneRadius drone into canvas, with position, radius and colour
     * @param droneCanvasObject
     */
    public void drawDrone(droneCanvas droneCanvasObject) {
        droneCanvasObject.drawCircle(x, y, radian, colour);
    }
  
    /**
     * Method 
     * is an object at contactDroneX,contactDroneY size contactDroneRadius colliding with this ball/drone
     * @param contactDroneX
     * @param contactDroneY
     * @param contactDroneRadius
     * @return true if drone is contactMade else false
     */
    public boolean contactMade(double contactDroneX, double contactDroneY, double contactDroneRadius) {
        return (contactDroneX-x)*(contactDroneX-x) + (contactDroneY-y)*(contactDroneY-y) < (contactDroneRadius+radian)*(contactDroneRadius+radian);
    }

    /**
     * Method to check if drone has contactMade another drone
     * @param contactDrone
     * @return true if a contactMade is made
     */
    public boolean contactMaking (Drone contactDrone) {
        return contactMade(contactDrone.getDroneXPos(), contactDrone.getDroneYPos(), contactDrone.getDroneRadius());
    }
    
    /**
     * Getter method to return x position of drone
     * @return x position
     */
    public double getDroneXPos() {
    	return x;
    }
    
    /**
     * Getter method to return y position of drone
     * @return y position
     */
    public double getDroneYPos() {
    	return y;
    }
    
    /**
     * Getter method to return radius of drone
     * @return radius
     */
    public double getDroneRadius() {
    	return radian;
    }
    
    /**
     * Return UID of drone
     * @return UID
     */
    public int getDroneID() {
    	return droneUID;
    }
    
    /**
     * Set drones new x and y positions
     * @param newXPos
     * @param newYPos
     */
    public void setNewXY(double newXPos, double newYPos) {
        x = newXPos;
        y = newYPos;
    }
    
    /**
     * Returns Drone string to display on object
     * @return uid & Drone
     */
    protected String getEntityString() {
        return droneUID + " Drone";
    }
    
    /**
     * Return entity name and x and y position
     * @return entity name & x position & y position
     */
    public String toString() {
        return getEntityString()+" is at x:"+Math.round(x)+", y:"+Math.round(y);
    }
}

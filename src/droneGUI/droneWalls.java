package droneGUI;

/**
 * Class to create drone walls.
 * Allowing for entities to bounce off of.
 * @author Jamie
 * @version 1.5.2
 */

public class droneWalls extends Drone {
	/**
	 * Constructor to draw wall entity with parsed variables and colour
	 * @param xWallPos
	 * @param yWallPos
	 * @param radiusWall
	 * @param droneCanvasObject
	 * @param droneArenaObject
	 */

    public droneWalls(double xWallPos, double yWallPos, double radiusWall) {
        super(xWallPos, yWallPos, radiusWall);
        // Set colour variable to white
        colour = "WHITE";
    }
    
    /** 
     * Needed for abstract class. No functionality
     */
    protected void droneAngleCheck(droneArena droneArenaObject) {}

    /** 
     * Needed for abstract class. No functionality
     */
    protected void changeDronePos() {}

    /**
	 * Method to draw circle into arena canvas object
	 * @param droneCanvasObject
	 */
    public void drawDrone(droneCanvas droneCanvasObject) {
        droneCanvasObject.drawCircle(x, y, radian, colour);
    }
    
    /** 
     * Returns Wall string to display on object
     * @return uid & Wall
     */    
    protected String getEntityString() {
        return droneUID + " Wall";
    }
}

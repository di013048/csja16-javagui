package droneGUI;

import java.util.Random;

/**
 * Class to create planet entities.
 * @author Jamie
 * @version 1.5.2
 */
public class planetEntities extends Drone {
	// Declaring health, direction, speed and count variables for planets
    private int health;
    private double speed = 2;
    // direction 1 = right, -1 = left
    private int direction = 1;
    private int count = 0;

    /**
     * Constructor to create planet entity based on xPlanetPos, yPlanetPos and radiusPlanet.
     * @param xPlanetPos
     * @param yPlanetPos
     * @param radiusPlanet
     */
    public planetEntities(double xPlanetPos, double yPlanetPos, double radiusPlanet) {
        super(xPlanetPos, yPlanetPos, radiusPlanet);
        // set planet health to 100
        health = 100;
        // set planet colour to aqua
        colour = "AQUA";
    }
    
    /**
     * Change planet trajectory
     */
    @Override
    protected void changeDronePos() {
    	// Generate random number
    	Random randInt = new Random();
    	// If count is a multiple of 5
    	if(count % 5 == 0){
    		// Change speed to random number with the range 0-5
    		x += randInt.nextInt(5) * direction;
    	} else {
    		// Else change y position to speed multiplied by direction
    		y += speed * direction;
    	}
    	// Increment count by 1
    	count++;
    }  

    /**
     * Check to see if planet is hit by drone
     * @param droneArenaObject
     */
    @Override
    protected void droneAngleCheck(droneArena droneArenaObject) {
    	// If planet is touched by drone entity
    	if (droneArenaObject.checkHit(this)) {
    		// If health is equal to half of original health
    		if (health != 0) {
    			// Add another planet to the canvas
    			health--;
    		} else { // Else decrement health by 1
    			health = 100;
    		}
        }
    	// If planet is in arena border
        if (droneArenaObject.droneIsInBorder(this) == false) {
        	// Change direction
        	direction = -direction;
        }
    }
    
    /**
     * Draw drone using droneCanvasObject
     * Display planet position and health
     */
    public void drawDrone(droneCanvas droneCanvasObject) {
        super.drawDrone(droneCanvasObject);
        // Display planet information to interface
        droneCanvasObject.showInt(x, y, health);
    }
    
    /**
     * Returns Planet string to display on object
     * @return uid & planet
     */
    protected String getEntityString() {
        return droneUID + " Planet";
    }
}

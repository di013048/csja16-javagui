package droneGUI;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;

/**
 * Class to create a canvas object to allow the simulation to work
 * @aurhor Jamie
 * @version 1.5.2
 */

public class droneCanvas {
	// Set canvas size to 500
	private int ySize, xSize = 500;
	// Initialise canvas object
    GraphicsContext droneCanvasObject;

    /**
     * Default constructor to set new graphics context and size of canvas
     * @param newDroneCanvasObject
     * @param newXSize
     * @param newYSize
     */
    public droneCanvas(GraphicsContext newDroneCanvasObject, int newXSize, int newYSize) {
        droneCanvasObject = newDroneCanvasObject;
        xSize = newXSize;
        ySize = newYSize;
    }

    /**
     * Method allows filling of canvas colour
     * @param newColour
     */
    public void setFillCanvasFill(String newColour) {
        droneCanvasObject.setFill(Color.valueOf(newColour));
    }
    
    /**
     * Empty canvas
     * By setting colour to black
     * And to fill rectangle
     */
    public void clearCanvas() {
        droneCanvasObject.setFill(Color.BLACK);
        droneCanvasObject.fillRect(0, 0, xSize, ySize);
    }
    
    /**
     * Draw circle using position circleRadiusius and colour
     * @param circleXPos
     * @param circleYPos
     * @param circleRadius
     * @param colour
     */
    public void drawCircle(double circleXPos, double circleYPos, double circleRadius, String colour) {
    	// Set circle colour
    	setFillCanvasFill(colour);
    	// draw circle with 240 degrees
        droneCanvasObject.fillArc(circleXPos-circleRadius, circleYPos-circleRadius, circleRadius*2, circleRadius*2, 300, 285, ArcType.ROUND);
    }
    
    /**
     * Draw rectangle using colour, top left, top right position and width and height
     * @param rectangleWidth
     * @param rectangleHeight
     * @param rectangeRadius
     */
    public void drawRectangle(double rectangleWidth, double rectangleHeight, double rectangeRadius, String newColour) {
    	// Set rectangle colour
    	setFillCanvasFill(newColour);
    	// draw rectangle
    	droneCanvasObject.fillRect(rectangleWidth-rectangeRadius, rectangleHeight-rectangeRadius, rectangeRadius*2, rectangeRadius*2);
    }
    
    /**
     * Draw text to entity with health displayed in center
     * @param entityXPos
     * @param entityYPos
     * @param entityHealth
     */
    public void drawText (double entityXPos, double entityYPos, String entityHealth) {
    	// Set text colour to red
    	droneCanvasObject.setFill(Color.RED);
    	// Center text alignment
        droneCanvasObject.setTextAlign(TextAlignment.CENTER);
        // Center text vertically
        droneCanvasObject.setTextBaseline(VPos.CENTER);							
        // Draw health string onto entity
        droneCanvasObject.fillText(entityHealth, entityXPos, entityYPos);
    }

    /**
     * Getter method to return x size of canvas
     * @return xSize
     */
    public int getXCanvasSize() {
        return xSize;
    }
    
    /**
     * Getter method to return y size of canvas
     * @return ySize
     */
    public int getYCanvasSize() {
        return ySize;
    }
    
    /**
     * Draw entityString onto canvas with position entityXPos and entityYPos with the string entityString
     * @param entitentityYPosXPos
     * @param entityYPos
     * @param entityString
     */
    public void showInt(double entityXPos, double entityYPos, int entityString) {
        drawText(entityXPos, entityYPos, Integer.toString(entityString));
    }
}

package droneGUI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class to create arena
 * @aurhor Jamie
 * @version 1.5.2
 */
public class droneArena implements Serializable {
	// Initialise arena size
    private int xSize, ySize;
    // Initialise array list of all drones
    private ArrayList<Drone> droneArrayList;
    
    /**
     * Default constructor to create arean of default size
     */
    droneArena() {
    	// Default size arena creation
        this(500, 500);
    }
    
    /**
     * Constructor override to create new arena with given size
     * Adds player, drones, planets and walls to the arena.
     * @param xS
     * @param yS
     */
    droneArena(int arenaXSize, int arenaYSize) {
    	// Declare number of walls
    	int numOfWalls = 5;
    	// Declare number of planets
    	int numOfPlanets = 2;
    	// Declare arena sizes based on parsed values
        xSize = arenaXSize; ySize = arenaYSize;
        // Initialise drone list
        droneArrayList = new ArrayList<Drone>();
        // Add planet to arena
        addPlanets(numOfPlanets);
        // Add number of walls to arena
        addWalls(numOfWalls);
        // Adds player drone to arena
        droneArrayList.add(new droneBlocker(arenaXSize/2, arenaYSize-20, 20));
        // Add drones to arena
        droneArrayList.add(new droneEntities(arenaXSize/2, arenaYSize*0.6, 10, 40, 4));
        
    }
    
    /**
     * Return list of collated strings containing information for each drone
     * @return
     */
    public ArrayList<String> droneCollation() {
    	// Declare empty array list
        ArrayList<String> collatedArrayList = new ArrayList<>();
        for (Drone element : droneArrayList) {
        	// Add toString result to collated array list array list
        	collatedArrayList.add(element.toString());
        }
        // Return drone information array
        return collatedArrayList;
    }
    
    /**
     * Adds drone to drone arena with given position
     */
    public void droneAdder() {
    	// Add drone to drone array list with given position and radius
        droneArrayList.add(new droneEntities(xSize/2, ySize*0.1, 5, 20, 4));
    }
    
    /**
     * Method to add planets to arena, based on number of planets defined
     * @param numOfPlanets
     */
    private void addPlanets(int numOfPlanets) {
    	// For each number of planet iteration
    	for(int i = 0; i < numOfPlanets; i++) {
    		// Declare random integer object
	    	Random randInt = new Random();
	    	// Declare x as a random integer with the maximum number of arena width
	    	int x = randInt.nextInt(xSize);
	    	// Declare y as a random integer with the maximum number of arena height
	    	int y = randInt.nextInt(ySize);
	    	// Declare r as random number in the range from 10 to 40
	    	int r = (int)(Math.random()*(40-10+1)+10);
	    	// Add planet to arena based on x and y position with radius r
	    	droneArrayList.add(new planetEntities(x, y, r));
    	}
    }
    
    /**
     * Method to add walls to arena, based on number of walls defined
     * @param numOfWalls
     */
    private void addWalls(int numOfWalls) {
    	// For each number of wall iteration
    	for(int i = 0; i < numOfWalls; i++) {
    		// Declare x as a random integer with the maximum number of arena width
    		Random randInt = new Random();
    		// Declare x as a random integer with the maximum number of arena width
        	int x = randInt.nextInt(xSize);
        	// Declare y as a random integer with the maximum number of arena height
        	int y = randInt.nextInt(ySize);
        	// Declare r as random number in the range from 5 to 30
        	int r = (int)(Math.random()*(30-5+1)+5);
        	// Add wall to arena based on x and y position with radius r
        	droneArrayList.add(new droneWalls(x, y, r));
    	}
    }
    
    /**
     * Check all drones, change angle if required
     */
    public void droneAnglesCheck() {
    	// Iteration over each element in drone list
        for (Drone element : droneArrayList) {
        	// Check drone iteration
        	element.droneAngleCheck(this);
        }
    }
    
    /**
     * Draw drones to canvas object
     * @param droneCanvasObject
     */
    public void drawArenaToCanvas(droneCanvas droneCanvasObject) {
        for (Drone element : droneArrayList) {
        	// Draw drones to drone canvas
        	element.drawDrone(droneCanvasObject);
        }
    }
    
    /**
     * Adjust drones
     */
    public void changeDronePosition() {
    	// Iteration over each element in drone list
        for (Drone element : droneArrayList) {
        	// Adjust drone iteration element
        	element.changeDronePos();
        }
    }
    
    /**
     * Check if player has been hit by drone
     * @param target
     * @return boolean
     */
    public boolean checkHit(Drone currentDrone) {
    	// Declare boolean to see if contact has been made
        boolean contactMade = false;
        // Iteration over each drone in the array list
        for (Drone element : droneArrayList) {
        	// If there is an instance of droneEntities and contactMaking method returns true
        	if (element instanceof droneEntities && element.contactMaking(currentDrone)){
        		// Declare contact made as true
        		contactMade = true;
        	}
        }
        // Return contact made boolean
        return contactMade;
    }
    
    /**
     * Create player instance and change position
     * @param xplayerXPosition
     * @param playerYPosition
     */
    public void playerInstance(double playerXPosition, double playerYPosition) {
    	// Iteration over each element in drone list
        for (Drone element : droneArrayList) {
        	// If there is an instance of player blocker
            if (element instanceof droneBlocker) {
            	// Set new player position to x and y coordinates
            	element.setNewXY(playerXPosition, playerYPosition);
            }
        }
    }
       
    /**
     * Check if drone element is in the border
     * @param droneElement
     * @return booleans
     */
    public boolean droneIsInBorder(Drone droneElement) {
    	boolean droneInBorder = !(
    			droneElement.getDroneXPos()+droneElement.getDroneRadius()>xSize|| 
    			droneElement.getDroneYPos()+droneElement.getDroneRadius()>ySize||
    			droneElement.getDroneXPos()-droneElement.getDroneRadius()<0|| 
    			droneElement.getDroneYPos()-droneElement.getDroneRadius()<0    			
    	);
    	return droneInBorder;
    }
    
    /**
     * Checks drone angle and detects if drone hits a wall or another drone
     * @param x
     * @param y
     * @param rad
     * @param ang
     * @param notID
     * @return trajectoryAngle
     */
    public double CheckDroneAngle(int duplicateUID, double rad, double x, double y, double trajectoryAngle) {
    	// Iteration over each drone element in drone list
        for (Drone element : droneArrayList) {
        	// If drone id is not duplicate
        	if (element.getDroneID() != duplicateUID) {
        		// If drone has made contact with wall or entity
        		if (element.contactMade(x, y, rad)) {
        			// Calculate inverse tangent function
        			trajectoryAngle = 180*Math.atan2(y-element.getDroneYPos(), x-element.getDroneXPos())/Math.PI;
        		}
        	}
        }
    	// If drone y position is less than radius or y is greater than arena size plus radius
        if (y > ySize - rad || y < rad) {
        	// Inverse trajectory angle by taking away angle from 0
        	trajectoryAngle = 0 - trajectoryAngle;
        }
    	// If drone x position is less than radius or x is greater than arena size minus radius
        if (x < rad || x > xSize - rad) {
        	// Inverse trajectory angle by 180
        	trajectoryAngle = 180 - trajectoryAngle;
        }
        // Return calculated trajectory angle
        return trajectoryAngle;		
    }
}
